var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var randomWord = require('random-word');

app.use('/assets',express.static(__dirname + '/assets'));
app.use('/build',express.static(__dirname + '/build'));

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});

const PORT = process.env.PORT || 8080;

server.listen(PORT,function(){ // Listens to port 8081
    console.log('Listening on '+PORT);
});

server.lastPlayerID = 0; // Keep track of the last id assigned to a new player

io.on('connection',function(socket){
    socket.emit('client_connected');

    socket.on('newplayer',function(){
        console.log("new player connected");
        socket.player = {
            id: socket.id,
            name: `${randomWord()} Stoner`,
            x: randomInt(100,400),
            y: randomInt(100,400)
        };
        socket.emit('allplayers',getAllPlayers());
        socket.broadcast.emit('newplayer',socket.player);

        //send player location to other players
        socket.on('move',function(data){
            socket.player.x = data.x;
            socket.player.y = data.y;
            socket.broadcast.emit('move',socket.player);
        });
        
        //user disconnected
        socket.on('disconnect',function(){
            io.emit('remove',socket.player.id);
        });

        //announce message to others
        socket.on("sendmsg", function(data){
            io.emit('incomingMsg', data);
        });

    });
});

function getAllPlayers(){
    var players = [];
    Object.keys(io.sockets.connected).forEach(function(socketID){
        var player = io.sockets.connected[socketID].player;
        if(player) players.push(player);
    });
    return players;
}

function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}