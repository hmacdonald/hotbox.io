var path = require('path');
var pathToPhaser = path.join(__dirname, '/node_modules/phaser/');
var phaser = path.join(pathToPhaser, 'dist/phaser.js');

const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    devServer: {
        contentBase: path.resolve(__dirname, './'),
        publicPath: '/build/',
        host: '127.0.0.1',
        port: 3000,
        open: true
    },
    resolve: {
        alias: {
          phaser: phaser,
        }
      }
});