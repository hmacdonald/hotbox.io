import * as io from 'socket.io-client';
import Player from './objects/Player';
export default class Client {
    public socket: SocketIOClient.Socket;
    public otherPlayers: PlayerMap = {};
    public localPlayers: PlayerMap = {};

    constructor() {
        this.socket = io();
        console.log(`ID: ${this.socket.id}`);
        setTimeout(() => {
            console.log(`ID: ${this.socket.id}`);
        }, 100)

    }

    public askNewPlayer() {
        this.socket.emit('newplayer');
    }

    public addNewPlayer(id: number, x: number, y: number, name: string, scene: Phaser.Scene): PlayerMap {

        this.otherPlayers[id] = new Player({
            key: 'player',
            scene: scene,
            x: x,
            y: y,
            name: `${name}`,
            isLocal: false,
        });

        return this.otherPlayers;
    }

    public addLocalPlayer(id: number, x: number, y: number, name: string, onMove: (x: number, y: number) => {}, scene: Phaser.Scene): PlayerMap {
        this.localPlayers[id] = new Player({
            key: 'player',
            scene: scene,
            x: x,
            y: y,
            name: `${name}`,
            isLocal: true,
            onMove: onMove,
        });

        return this.localPlayers;
    }

    public movePlayer(id: number, x: number, y: number) {
        if (this.otherPlayers[id] !== undefined) {
            this.otherPlayers[id].setPosition(x, y);
        }
    }

    public removePlayer(id: number) {
        console.log(`${this.otherPlayers[id].name} logged off`)
        this.otherPlayers[id].nameLabel.destroy();
        this.otherPlayers[id].destroy();
    }

    public showMessage(playerId: number, text: string) {
        console.log(`${this.otherPlayers[playerId].name} sent message ${text}`);
    }
}

export interface PlayerMap {
    [id: number]: Player
}