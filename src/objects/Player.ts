
export default class Player extends Phaser.GameObjects.Sprite {
    private cursors: IMoveMentKeys;
    private camera: Phaser.Cameras.Scene2D.Camera;
    private inputAxes: { x: number, y: number; } = { x: 0, y: 0 };
    private speed: number = 0.1;
    public nameLabel: Phaser.GameObjects.Text;
    private isLocal: boolean = false;
    private config: IPlayerConfig;
    private chatFocused: boolean = true;

    constructor(config: IPlayerConfig) {
        super(config.scene, config.x, config.y, config.key);
        this.config = config;
        this.scene = config.scene;
        this.initInput(config);
        this.nameLabel = this.scene.add.text(config.x, config.y - 75, config.name);
        this.nameLabel.setOrigin(0.5, 0.5);
        this.nameLabel.setFont("FuturaRenner")
        this.nameLabel.setFontSize(16);
        this.scene.add.existing(this);
        this.setDepth(20);
        this.isLocal = config.isLocal;
        if (this.isLocal) {
            console.log("New local player instantiated");

            this.camera = this.scene.sys.cameras.main;
            this.camera.startFollow(this, true);

        } else {
            console.log("New remote player instantiated " + config.name);
        }

    }



    public Update(time: number, deltaTime: number) {
        let didMove: boolean = true;
        if (this.isLocal && this.chatFocused) {
            if (this.cursors.down.isDown) {
                this.inputAxes.y = this.speed;
                this.anims.play('down', true);
            } else if (this.cursors.up.isDown) {
                this.inputAxes.y = -this.speed;
                this.anims.play('up', true);
            } else {
                this.inputAxes.y = 0;
            }

            if (this.cursors.right.isDown) {
                this.inputAxes.x = this.speed;
                this.anims.play('right', true);
            } else if (this.cursors.left.isDown) {
                this.inputAxes.x = -this.speed;
                this.anims.play('left', true);
            } else {
                this.inputAxes.x = 0;
            }

            if (this.inputAxes.x == 0 && this.inputAxes.y == 0) {
                this.anims.stop();
                didMove = false;
            }

            this.setPosition(this.x + this.inputAxes.x * deltaTime, this.y + this.inputAxes.y * deltaTime);
            if (didMove) {
                this.config.onMove(this.x, this.y);

            }
        }
        this.nameLabel.setPosition(this.x, this.y - 25);
    }

    private initInput(config: IPlayerConfig): void {
        document.getElementById("message-text").addEventListener("blur", () => {
            this.chatFocused = true;
        });
        document.getElementById("message-text").addEventListener("focus", () => {
            this.chatFocused = false;
        });
        this.cursors = {
            down: config.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S),
            left: config.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A),
            right: config.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D),
            up: config.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W),
        };
    }
}

export interface IPlayerConfig {
    scene: Phaser.Scene;
    x: number;
    y: number;
    key: string;
    name: string;
    isLocal: boolean;
    onMove?: (x: number, y: number) => {};
}

interface IMoveMentKeys {
    up: Phaser.Input.Keyboard.Key;
    down: Phaser.Input.Keyboard.Key;
    left: Phaser.Input.Keyboard.Key;
    right: Phaser.Input.Keyboard.Key;
}
