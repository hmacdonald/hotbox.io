/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 Digitsensitive
 * @license      Digitsensitive
 */

/// <reference path="./phaser.d.ts"/>
import 'phaser';
import '../styles/styles.scss';
import { MainScene } from "./scenes/mainScene";

// main game configuration
const config: GameConfig = {
    width: 900,
    height: 600,
    pixelArt: true,
    type: Phaser.AUTO,
    parent: "game",
    scene: MainScene,
    zoom: 1,
    physics: {
        default: "arcade",
        arcade: {
            gravity: { y: 200 }
        }
    }
};

// game class
export class Game extends Phaser.Game {
    constructor(config: GameConfig) {
        super(config);
        ;
    }
}

// when the page is loaded, create our game instance
window.onload = () => {
    var game = new Game(config);
};

declare global {
    interface Window {
        game: Phaser.Game;
    }
}