/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 Digitsensitive
 * @license      Digitsensitive
 */


import Client from '../Client';
import { require } from 'webpack';
import { IPlayerConfig } from '../objects/Player';
import { Display } from 'phaser';
export class MainScene extends Phaser.Scene {
    private map: Phaser.Tilemaps.Tilemap;
    public client: Client;
    loader: Phaser.GameObjects.Text;

    public tileMapLayers: Phaser.Tilemaps.StaticTilemapLayer[] = [];
    public tileLayers : string[] =  ["Background", "Layer 1","Layer 2","Foreground"];
    constructor() {
        super({
            key: 'MainScene',
        });
    }

    public preload(): void {
        this.load.spritesheet('player', '../assets/playerSpriteSheet.png', { frameWidth: 16, frameHeight: 16 });
        this.load.image('gameTiles', '../assets/basictiles.png');
        this.load.tilemapTiledJSON('level1', '../assets/world.json');
    }

    public create(): void {
        // initialize socket.io client
        this.initClient();
        // create GameObjects
        this.anims.create({
            key: "down",
            frames: this.anims.generateFrameNumbers('player', { start: 0, end: 2 }),
            frameRate: 6,
            repeat: -1
        });
        this.anims.create({
            key: "right",
            frames: this.anims.generateFrameNumbers('player', { start: 3, end: 5 }),
            frameRate: 6,
            repeat: -1
        });
        this.anims.create({
            key: "up",
            frames: this.anims.generateFrameNumbers('player', { start: 6, end: 8 }),
            frameRate: 6,
            repeat: -1
        });
        this.anims.create({
            key: "left",
            frames: this.anims.generateFrameNumbers('player', { start: 9, end: 11 }),
            frameRate: 6,
            repeat: -1
        });

        this.loader = this.add.text(this.sys.canvas.width / 2, this.sys.canvas.height / 2, "Connecting...");
        this.loader.setOrigin(0.5,0.5);
        this.loader.setFont("FuturaRenner");
        this.loader.setFontSize(16)

        this.client.socket.on('client_connected', () => {
            this.loader.destroy();
            this.map = this.add.tilemap('level1');
            this.loadAllLayers();
            this.client.askNewPlayer();
        });
    }

    public loadAllLayers() {
        const tileset = this.map.addTilesetImage('basictiles', 'gameTiles');
        this.tileLayers.forEach(name => {
            this.map.createStaticLayer(name, tileset, 0, 0);
        });

    }

    public update(t: number, dt: number): void {
        Object.keys(this.client.localPlayers).forEach(key => {
            this.client.localPlayers[key].Update(t, dt);
        });
        Object.keys(this.client.otherPlayers).forEach(key => {
            this.client.otherPlayers[key].Update(t, dt);
        });
    }

    private initClient() {
        this.client = new Client();
        this.client.socket.on('client_connected', () => {
            console.log("Client connected!");
            this.initNetworkEvents();
        });
    }

    private initNetworkEvents() {

        const playerMoved = (x: number, y: number) => {
            this.client.socket.emit('move', { x: x, y: y });
            return {};
        }

        this.client.socket.on('allplayers', data => {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                if (data[i].id !== this.client.socket.id) {
                    this.client.addNewPlayer(data[i].id, data[i].x, data[i].y, data[i].name, this);
                } else {
                    this.client.addLocalPlayer(data[i].id, data[i].x, data[i].y, data[i].name, playerMoved, this);
                }
            }
        });

        this.client.socket.on('newplayer', data => {
            console.log("new player", data);
            this.client.addNewPlayer(data.id, data.x, data.y, data.name, this);
        });

        this.client.socket.on('remove', data => {
            this.client.removePlayer(data);
        });

        this.client.socket.on('move', data => {
            this.client.movePlayer(data.id, data.x, data.y);
        });
    }
}
