### Install

Navigate to the folder and clone this repository.

`git clone https://hmacdonald@bitbucket.org/hmacdonald/hotbox.io.git`

### Build and Run

Building and running is as easy as the three following steps:

| Command | Description |
|---------|-------------|
| `npm install` | Install dependencies (folder: node_modules).|
| `npm run dev` | Perform a quick build (bundle.js) and start server.|
| `npm run build` | Perform a final build with minification.|
